# Trailer Microservice

This microservice lets you retrieve YouTube trailer URLs for movies in the Viaplay system.

## Requirements

To run this microservice you will need the following software installed on your system:

* Docker (at least version 1.12.0)
* Docker Compose (at least version 1.10.0)

## Setup & Starting services

You have to enter your TmDB API key in the `var/config.json` file:

```
...
  "tmdb": {
    ...
    "api_key": "<your API key here>"
  }
...
```

To start the services simply run the docker-compose command:

```
$ docker-compose up
```

To remove all volumes (sometimes useful if you want to prematurely dump the Redis cache without 
having to log into the Redis server) and destroy all containers:

```
$ docker-compose down
```

## REST Endpoints

If you run with the Docker setup, the base URL will be http://localhost:8888/

### GET /trailers?resource=<resource_url>

#### URL parameters

* **resource** (REQUIRED) - Should be in the form of the Resource URL to a Viaplay movie (example: https://content.viaplay.se/pc-se/film/grimsby-2016)

#### Response

This endpoint has the following JSON response:

**HTTP 200**:

```
{
    "trailerUrl": "<url>"
}
```

**HTTP 400**:

```
{
    "error": "<error message>"
}
```

## Architecture

### Overview

My implementation uses the following frameworks/libraries:

* Express (HTTP server)
* Bluebird (Promise implementation that I prefer over the NodeJS default one that ships with 7.5.0)
* Electrolyte (Very nice IoC)
* Redis (for.... Redis!)
* Request + Request Promise (HTTP client)
* Winston (Logging)

I've implemented separation of concern the best I could with the time I spent on this:

* REST API clients (app/clients) are responsible for making REST requests and userialize them into models
* Models (app/models) are representations of entities retrieved through the REST APIs
* Services (app/services) aggregate data from REST API clients and deal with the caching of the results
* Smaller components (app/components), or pieces of code to instantiate for IoC container
* Handlers (app/web/handlers) are responsible for handling HTTP requests.

I've tried to use the asynchronous features of NodeJS in the most logical ways possible, evading the Promise anti-patterns
and making sure that all rejections are dealt with, and neatly propagage all the way up to the HTTP handlers.

I've implemented logging in all pieces of logic where it makes sense.

Caching is done with Redis - the result of each of the 3 API calls is individually cached so that potentially cascading/overlapping
calls can return individually (and also, the current implementation was the most straight forward)

### What's missing

* Unit tests - didn't have the time to properly read up on how to best approach unit tests with asyncrhonous modules.
* 

### Reliability/Availability & Problems

I haven't had the time to create a simple JMeter test suite to launch concurrent requests at the app, but here's some problems I anticipated:

* Currently the service requires the Redis server to always be available, ideally it should fall back on JS memory arrays. (SPOF)

* While successful results are cached in Redis, unsuccessful requests still launch API calls each request. (potential DoS/API rate limit exhaustion attack)

### Opportunity for improvements

These are in the order of importance:

* Unit tests
* CI to run ESLint + unit tests
* Integration tests
* Cache driver interaction can probably be abstracted to look more elegant
* Caching of results can be greatly optimized by storing the FINAL result as a cache for the first service call
* Error handling can probably be improved
* Refactors to more elegantly make use of .then().then().catch() promise resolution chaining.