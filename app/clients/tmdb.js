const Promise = require('bluebird');
const TmdbMovie = require('../models/tmdb_movie');
const YoutubeVideo = require('../models/youtube_video');

class TmdbClient {
  constructor(logger, settings, request) {
    this.logger = logger;
    this.request = request;

    this.baseUrl = settings.get('tmdb').base_url;
    this.apiKey = settings.get('tmdb').api_key;
  }

  getMovieByImdbId(imdbId) {
    return new Promise((resolve, reject) => {
      this.logger.debug(`Attempting to find movie ${imdbId} on TmDB`);

      this.request({
        uri: `${this.baseUrl}find/${imdbId}?api_key=${this.apiKey}&language=en-US&external_source=imdb_id`,
        json: true,
      }).then((jsonData) => {
        if (jsonData.movie_results.length === 0) {
          this.logger.debug(`Failed finding movie ${imdbId} on TmDB: No results found in search`);
          reject(new Error('Failed to find movie on TmDB'));
        }

        this.logger.debug(`Found movie ${imdbId} on TmDB`);

        resolve(new TmdbMovie({
          id: jsonData.movie_results[0].id,
        }));
      })
      .catch((e) => {
        this.logger.debug(`Failed finding movie ${imdbId} on TmDB: non-200 HTTP response`);
        this.logger.debug(e);

        reject(new Error('Failed to find movie on TmDB'));
      });
    });
  }

  getFirstVideoByMovieId(movieId, type, site) {
    return new Promise((resolve, reject) => {
      this.logger.debug(`Attempting to find videos for movie ${movieId} on TmDB`);

      this.request({
        uri: `${this.baseUrl}movie/${movieId}/videos?api_key=${this.apiKey}&language=en-US`,
        json: true,
      }).then((jsonData) => {
        const videos = jsonData.results
          .filter(video => video.type === type)
          .filter(video => video.site === site)
          .map(video => new YoutubeVideo(video.key));

        if (videos.length === 0) {
          this.logger.debug(`Failed finding movie ${movieId}'s videos on TmDB: no videos match criteria`);
          reject(new Error('Failed obtaining video for this resource'));
        }

        this.logger.debug(`Found video for movie ${movieId} on TmDB`);

        resolve(videos[0]);
      })
        .catch((e) => {
          this.logger.debug(`Failed finding movie ${movieId}'s videos on TmDB: non-200 HTTP response`);
          this.logger.debug(e);

          reject(new Error('Failed obtaining video for this resource'));
        });
    });
  }
}

module.exports = (logger, settings, request) => new TmdbClient(logger, settings, request);

module.exports['@singleton'] = true;
module.exports['@require'] = ['components/logger', 'components/settings', 'request-promise'];
