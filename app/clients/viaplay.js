const Promise = require('bluebird');
const ViaplayMovie = require('../models/viaplay_movie');

class ViaplayClient {
  constructor(logger, request) {
    this.logger = logger;
    this.request = request;
  }

  getResource(resourceUrl) {
    return new Promise((resolve, reject) => {
      this.logger.debug(`Attempting to load Viaplay resource ${resourceUrl}`);

      this.request({
        uri: resourceUrl,
        json: true,
      }).then((jsonData) => {
        const imdbId = jsonData._embedded['viaplay:blocks'][0]._embedded['viaplay:product'].content.imdb.id; // eslint-disable-line no-underscore-dangle

        if (imdbId !== undefined) {
          this.logger.debug(`Successfully loaded resource ${resourceUrl}`);
          resolve(new ViaplayMovie({
            imdbId,
            resourceUrl,
          }));
        } else {
          this.logger.debug(`Failed to loaded resource ${resourceUrl}: invalid JSON response`);
          reject(new Error('Loading the specified resource failed.'));
        }
      })
      .catch(() => {
        this.logger.debug(`Failed to loaded resource ${resourceUrl}: non-200 HTTP response`);
        reject(new Error('Loading the specified resource failed.'));
      });
    });
  }
}

module.exports = (logger, request) => new ViaplayClient(logger, request);

module.exports['@singleton'] = true;
module.exports['@require'] = ['components/logger', 'request-promise'];
