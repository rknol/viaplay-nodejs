const Promise = require('bluebird');

module.exports = (redis, settings) => {
  Promise.promisifyAll(redis.RedisClient.prototype);
  Promise.promisifyAll(redis.Multi.prototype);

  return redis.createClient(settings.get('redis'));
};

module.exports['@singleton'] = true;
module.exports['@require'] = ['redis', 'components/settings'];
