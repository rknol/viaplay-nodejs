const crypto = require('crypto');

class Hash {
  fromString(str) { // eslint-disable-line class-methods-use-this
    return crypto.createHash('md5').update(str).digest('hex');
  }
}

module.exports = () => new Hash();
module.exports['@singleton'] = true;
