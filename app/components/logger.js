module.exports = (winston, settings) => new winston.Logger({
  level: settings.get('logging').log_level,
  transports: [
    new (winston.transports.Console)(),
  ],
});

module.exports['@singleton'] = true;
module.exports['@require'] = ['winston', 'components/settings'];
