const fs = require('fs');

const CONF_FILE = 'var/config.json';

class Settings {
  constructor() {
    this.hash = {};
  }

  load(obj) {
    this.hash = obj;
  }

  set(key, val) {
    this.hash[key] = val;
  }

  get(key) {
    return this.hash[key];
  }
}

module.exports = () => {
  const settings = new Settings();

  if (fs.existsSync(CONF_FILE)) {
    const data = fs.readFileSync(CONF_FILE, 'utf8');
    const json = JSON.parse(data);

    settings.load(json);
  }

  return settings;
};

module.exports['@singleton'] = true;
