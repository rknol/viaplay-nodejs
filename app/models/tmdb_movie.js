class TmdbMovie {
  constructor(data) {
    this.data = data;
  }

  get id() {
    return this.data.id;
  }

  /**
   * Return value to be cached
   * @returns {String}
   */
  toCache() {
    return this.id;
  }

  /**
   * Instantiate model from cache value
   * @param {String} cacheVal
   * @returns {ViaplayMovie}
   */
  static fromCache(cacheVal) {
    return new TmdbMovie({
      id: cacheVal,
    });
  }
}

module.exports = TmdbMovie;
