class ViaplayMovie {
  constructor(data) {
    this.data = data;
  }

  get imdbId() {
    return this.data.imdbId;
  }

  /**
   * Return value to be cached
   * @returns {String}
   */
  toCache() {
    return this.imdbId;
  }

  /**
   * Instantiate model from cache value
   * @param {String} cacheVal
   * @returns {ViaplayMovie}
   */
  static fromCache(cacheVal) {
    return new ViaplayMovie({
      imdbId: cacheVal,
    });
  }
}

module.exports = ViaplayMovie;
