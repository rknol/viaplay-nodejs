class YoutubeVideo {
  constructor(key) {
    this.key = key;
  }

  get url() {
    return `https://www.youtube.com/watch?v=${this.key}`;
  }

  /**
   * Return value to be cached
   * @returns {String}
   */
  toCache() {
    return this.key;
  }

  /**
   * Instantiate model from cache value
   * @param {String} cacheVal
   * @returns {YoutubeVideo}
   */
  static fromCache(cacheVal) {
    return new YoutubeVideo(cacheVal);
  }
}

module.exports = YoutubeVideo;
