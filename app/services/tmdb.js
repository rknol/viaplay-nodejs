const TmdbMovie = require('../models/tmdb_movie');
const YoutubeVideo = require('../models/youtube_video');

class TmdbService {
  constructor(logger, tmdbClient, cache, hash) {
    this.logger = logger;
    this.client = tmdbClient;
    this.cache = cache;
    this.hash = hash;
  }

  getMovieByImdbId(imdbId) {
    return new Promise((resolve, reject) => {
      const hash = this.hash.fromString(imdbId);
      const cacheKey = `tmdb_${hash}`;

      this.cache.getAsync(cacheKey)
        .then((res) => {
          if (res === null) {
            this.logger.debug(`Got a cache miss for TmDB movie ${imdbId}`);
            this.client.getMovieByImdbId(imdbId)
              .then((movie) => {
                this.logger.debug(`Creating cache entry for TmDB movie ${imdbId}`);
                this.cache.set(cacheKey, movie.toCache());
                this.cache.expire(cacheKey, 600);

                resolve(movie);
              })
              .catch(e => reject(e));
          } else {
            this.logger.debug(`Got a cache hit for TmDB movie ${imdbId}`);
            resolve(TmdbMovie.fromCache(res));
          }
        })
        .catch(e => reject(e));
    });
  }

  getTrailerVideo(movie) {
    return new Promise((resolve, reject) => {
      const hash = this.hash.fromString(movie.id.toString());
      const cacheKey = `yt_${hash}`;

      this.cache.getAsync(cacheKey)
        .then((res) => {
          if (res === null) {
            this.logger.debug(`Got a cache miss for YouTube video for movie ${movie.id}`);
            this.client.getFirstVideoByMovieId(movie.id, 'Trailer', 'YouTube')
              .then((video) => {
                this.logger.debug(`Creating cache entry for YouTube video ${video.key}`);
                this.cache.set(cacheKey, video.toCache());
                this.cache.expire(cacheKey, 600);

                resolve(video);
              })
              .catch(e => reject(e));
          } else {
            this.logger.debug(`Got a cache hit for YouTube video ${res}`);
            resolve(YoutubeVideo.fromCache(res));
          }
        })
        .catch(e => reject(e));
    });
  }
}

module.exports = (logger, tmdbClient, cache, hash) => new TmdbService(
  logger, tmdbClient, cache, hash);

module.exports['@singleton'] = true;
module.exports['@require'] = ['components/logger', 'clients/tmdb', 'components/cache', 'components/hash'];
