const Promise = require('bluebird');
const ViaplayMovie = require('../models/viaplay_movie');

class ViaplayService {
  constructor(logger, viaplayClient, cache, hash) {
    this.logger = logger;
    this.client = viaplayClient;
    this.cache = cache;
    this.hash = hash;
  }

  validateResourceUrl(url) { // eslint-disable-line class-methods-use-this
    return new Promise((resolve, reject) => {
      if (url.match(/https:\/\/content.viaplay.se\/pc-se\/(.*)/) !== null) {
        resolve(true);
      } else {
        reject(new Error('Resource provided is not valid'));
      }
    });
  }

  getMovieByResourceUrl(url) {
    return new Promise((resolve, reject) => {
      const hash = this.hash.fromString(url);
      const cacheKey = `vp_${hash}`;

      this.cache.getAsync(cacheKey)
        .then((res) => {
          if (res === null) {
            this.logger.debug(`Got a cache miss for resource ${url}`);
            this.client.getResource(url)
              .then((movie) => {
                this.logger.debug(`Creating cache entry for resource ${url}`);
                this.cache.set(cacheKey, movie.toCache());
                this.cache.expire(cacheKey, 600);

                resolve(movie);
              })
              .catch(e => reject(e));
          } else {
            this.logger.debug(`Got a cache hit for resource ${url}`);
            resolve(ViaplayMovie.fromCache(res));
          }
        })
        .catch(e => reject(e));
    });
  }
}

module.exports = (logger, viaplayClient, cache, hash) => new ViaplayService(
  logger, viaplayClient, cache, hash);

module.exports['@singleton'] = true;
module.exports['@require'] = ['components/logger', 'clients/viaplay', 'components/cache', 'components/hash'];
