const http = require('http');
const Promise = require('bluebird');
const express = require('express');

module.exports = (router, logger, settings) => {
  const app = express();
  app.use(router);

  return new Promise((resolve) => {
    const server = http.createServer(app);
    server.listen(settings.get('express').port, resolve.bind(null, server));
  }).then((server) => {
    const addr = server.address();
    logger.info(`HTTP server listening on http://${addr.address}:${addr.port}`);
  });
};

module.exports['@singleton'] = true;
module.exports['@require'] = ['web/router', 'components/logger', 'components/settings'];
