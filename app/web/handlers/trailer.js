module.exports = (logger, viaplayService, tmdbService) => {
  const logRequest = (req, res, next) => {
    logger.info('GET Request made to /trailer');
    next();
  };

  const validate = (req, res, next) => {
    if (req.query.resource === undefined) {
      return res.json({
        error: 101,
        msg: 'Query parameter \'resource\' is missing',
      });
    }

    return next();
  };

  const response = (req, res) => {
    const url = req.query.resource;

    viaplayService.validateResourceUrl(url)
      .then(() => viaplayService.getMovieByResourceUrl(url))
      .then(viaplayMovie => tmdbService.getMovieByImdbId(viaplayMovie.imdbId))
      .then(tmdbMovie => tmdbService.getTrailerVideo(tmdbMovie))
      .then(trailerVideo => res.json({
        trailerUrl: trailerVideo.url,
      }))
      .catch((e) => {
        res.status = 400; // eslint-disable-line no-param-reassign
        res.json({
          error: e.message,
        });
      });
  };

  /**
   * GET /trailer
   */
  return [logRequest, validate, response];
};

module.exports['@require'] = ['components/logger', 'services/viaplay', 'services/tmdb'];
