module.exports = function routes(express, trailerHandler) {
  const router = express();

  router.get('/trailer', trailerHandler);

  return router;
};

module.exports['@singleton'] = true;
module.exports['@require'] = ['express', 'web/handlers/trailer'];
