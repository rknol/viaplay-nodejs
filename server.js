const IoC = require('electrolyte');

IoC.use(IoC.dir('app'));
IoC.use(IoC.node_modules());

IoC.create('web/app')
  .then(() => {
    // Web application is running.
  })
.catch(e => console.log(e)); // eslint-disable-line no-console
